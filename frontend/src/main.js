import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./plugins/element.js";
import { AxiosInterceptors } from "./plugins/axios";
import VueScrollTo from "vue-scrollto";
import "bootstrap/dist/css/bootstrap.min.css";
import "element-theme-chalk";
import "./sass/app.scss";

Vue.use(VueScrollTo);
Vue.use(AxiosInterceptors);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
