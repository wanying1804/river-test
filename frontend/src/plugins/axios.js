import axios from "axios";

const AxiosInterceptors = {
  install() {
    axios.defaults.baseURL = process.env.VUE_APP_API_URL;
    axios.defaults.headers.common = {
      Accept: "application/json"
    };
    axios.interceptors.response.use(
      response => {
        return response;
      },
      async error => {
        throw error;
      }
    );
  }
};

export { AxiosInterceptors };
