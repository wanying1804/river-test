<?php

use App\Models\Booking;
use Illuminate\Database\Seeder;


/**
 * Class BookingSeeder
 */
class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Booking::class, 2)->create();
    }
}
