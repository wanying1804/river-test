<?php
namespace App\Http\Controllers;

use App\Http\Requests\Bookings\AddBookingRequest;
use App\Http\Resources\BookingResource;
use App\Models\Booking;
use Carbon\Carbon;
use Request;

/**
 * Class BookingController
 * @package App\Http\Controllers
 */
class BookingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
       return BookingResource::collection(Booking::orderByDesc('id')->get());
    }

    /**
     * @param AddBookingRequest $request
     * @return BookingResource
     */
    public function store(AddBookingRequest $request)
    {
        $booking_at = Carbon::createFromFormat('Y-m-d H:i', $request->date . ' ' . $request->time);
        $booking = Booking::create([
           'name' => $request->name,
           'booking_at' => $booking_at,
           'message' => $request->message,
        ]);

        return new BookingResource($booking);
    }
}
