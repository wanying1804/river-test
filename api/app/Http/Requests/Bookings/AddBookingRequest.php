<?php
namespace App\Http\Requests\Bookings;

use App\Http\Requests\FormRequest;

/**
 * Class AddMeetingRequest
 * @package Ac\Meetings\Http\Requests\Meetings
 */
class AddBookingRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'       => 'required|min:2|max:255',
            'date'       => 'required|date_format:Y-m-d',
            'time'       => 'required|date_format:H:i',
            'message'    => 'required',
        ];
    }
}
