<?php
namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'booking_at'    => $this->booking_at ? $this->booking_at->toDateTimeString() : null,
            'message'       => $this->message
        ];
    }
}
